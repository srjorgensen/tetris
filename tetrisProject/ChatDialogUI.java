package tetrisProject;
// $Id: ChatDialogUI.java,v 1.3 2018/04/06 21:32:56 cheon Exp $

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

/** A simple chat dialog. */
@SuppressWarnings("serial")
public class ChatDialogUI extends JDialog {
    
    /** Default dimension of chat dialogs. */
    private final static Dimension DIMENSION = new Dimension(400, 400);
    
    private JButton connectButton;
    private JButton hostButton;
    private JButton sendButton;
    private JTextField serverEdit;
    private JTextField portEdit;
    private JTextArea msgDisplay;
    private JTextField msgEdit;
    
    private Controller control = null; 
    private String hostName = " ";
    private String clientName = " ";
    private String userName = "Nameless";
    private String userMode = " ";

    
    /** Create a main dialog. */
    public ChatDialogUI(Controller c) { 
        this(DIMENSION);
        this.control = c;
    }
    
    public String getUserMode() {
    	return this.userMode;
    }
    
    /** Create a main dialog of the given dimension. */
    public ChatDialogUI(Dimension dim) {
        super((JFrame) null, "JavaChat");
        configureGui();
        setSize(dim);
        //setResizable(false);
        connectButton.addActionListener(this::connectClicked);
        hostButton.addActionListener(this::hostClicked);
        sendButton.addActionListener(this::sendClicked);
        setLocationRelativeTo(null);
    }
    
    /** Configure UI of this dialog. */
    private void configureGui() {
        JPanel connectPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        connectButton = new JButton("Connect");
        hostButton = new JButton("Host");
        
        connectButton.setFocusPainted(false);
        hostButton.setFocusPainted(false);
        
        serverEdit = new JTextField("localhost", 18);
        portEdit = new JTextField("8000", 4);
        connectPanel.add(hostButton);
        connectPanel.add(connectButton);
        connectPanel.add(serverEdit);
        connectPanel.add(portEdit);
        
        msgDisplay = new JTextArea(10, 30);
        msgDisplay.setEditable(false);
        DefaultCaret caret = (DefaultCaret)msgDisplay.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE); // autoscroll
        JScrollPane msgScrollPane = new JScrollPane(msgDisplay);

        JPanel sendPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        msgEdit = new JTextField("Enter a message.", 27);
        sendButton = new JButton("Send");
        sendButton.setFocusPainted(false);
        sendPanel.add(msgEdit);
        sendPanel.add(sendButton);
        
        setLayout(new BorderLayout());
        add(connectPanel, BorderLayout.NORTH);
        add(msgScrollPane, BorderLayout.CENTER);
        add(sendPanel, BorderLayout.SOUTH);
    }
    
    /** Callback to be called when the connect button is clicked. */
    private void connectClicked(ActionEvent event) {
        //--
        //-- WRITE YOUR CODE HERE
        //--
    	
    	
    	if(event.getSource() != null) {
    		
    		JFrame frame = new JFrame();
            Object name = JOptionPane.showInputDialog(frame, "What is your name:");
            
            clientName  = (String) name;
            userName = clientName; 
            
            getIPAndPortText();
            control.dialogButton("connect", clientName);
            
    		
    	}
    }
    
    /** Callback to be called when the connect button is clicked. */
    private void hostClicked(ActionEvent event) {
        //--
        //-- WRITE YOUR CODE HERE
        //--
    	if(event.getSource() != null) {
    		
    		
	    	printIP();
	    	JFrame frame = new JFrame();
	        Object name = JOptionPane.showInputDialog(frame, "What is your name:");
	        
	        hostName = String.valueOf(name);
	        userName = hostName; 
       
	        getIPAndPortText();
    		control.dialogButton("host", hostName);
    		
    	}
    }
    
    /**
     * Callback to when send button is clicked
     * @param event
     */
    private void sendClicked(ActionEvent event) {
    	
    	if(event.getSource() != null) {
    		printMessage(userName + ": " + msgEdit.getText());
    		control.na.writeMessage(userName + ": " + msgEdit.getText());
    	}
    }
    
    /**
     * Prints IP address 
     */
    private void printIP() {
    	
    	try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			printMessage("IP: " + inetAddress.getHostAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			printMessage("Couldnt get IP address: " + e.getMessage());
		}
    	
    }
    
    /**
     * Prints message onto message display 
     * @param s
     */
    public void printMessage(String s) {
    	msgDisplay.append(s + "\n");
    }
    
    /**
     * Gets host and port
     */
    public void getIPAndPortText() {
    	Object host = serverEdit.getText();
		Object port = portEdit.getText();
		control.setHostPort(host, port);
    	
    }

    /** Show the given message in a dialog. */
    private void warn(String msg) {
        JOptionPane.showMessageDialog(this, msg, "JavaChat", 
                JOptionPane.PLAIN_MESSAGE);        
    }
}
