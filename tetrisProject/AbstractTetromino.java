package tetrisProject;

import tetrisProject.TetrominoFactory.TetrominoEnum;

public abstract class AbstractTetromino {
	/**
	 * Starting row
	 */
	private final int ROW = 0;
	/**
	 * Starting column
	 */
    private final int COL = 4;
	
	/**
	 * Tetromino (x,y) coordinates
	 */
	protected int x, y; 
	
	/**
	 * Contains all possible configurations of a tetromino
	 */
	private int[][][] allShapePositions;
	
	/**
	 * Contains the current tetromino configuration
	 */
	private int[][] singleShapePosition;
	
	/**
	 * Contains the current tetromino configuration aligned to the point (x,y) on board
	 */
	private int[][] alignedShapePosition;
	
	/**
	 * Board character
	 */
	private char boardChar = ' ';
	
	/**
	 * Used in rotation methods to get the desired tetromino configuration
	 */
	private int rotateOffset; 
	
	private TetrominoEnum tetrominoEnum; 
	
	// --=== SETTERS AND GETTERS ===---
	/**
	 * Initializes starting row and col for tetromino
	 */
	protected void setInitialPoint() {
		this.x = ROW;
		this.y = COL;
	}
	/**
	 * Sets the x value of point
	 * @param val
	 */
	public void setX(int val) {
		this.x = val;
		
	}
	/**
	 * Gets the x value of point
	 * @return int
	 */
	public int getX(){
		return this.x; 
	}
	
	
	/**
	 * Sets the y value of point
	 * @param val
	 */
	public void setY(int val) {
		this.y = val;
	}
	/**
	 * Gets the y value of point
	 * @return int
	 */
	public int getY() {
		return this.y; 
	}
	
	
	/**
	 * Sets allShapePositions
	 */
	public void setAllPositions(int[][][] shapePos) {
		this.allShapePositions = shapePos; 
	}
	
	/**
	 * Sets singleshapoePosition
	 * @param shapePos
	 */
	public void setSingleShape(int[][] shapePos) {
		this.singleShapePosition = shapePos; 
	}
	/**
	 * Gets singleShapePosition
	 * @return
	 */
	public int[][] getSingleShape(){
		return this.singleShapePosition; 
	}
	
	
	/**
	 * Sets alignedShapePosition
	 * @param alignedPos
	 */
	public void setAlignedShapePosition(int[][] alignedPos) {
		this.alignedShapePosition = alignedPos; 
	}
	
	/**
	 * Gets alignedShapePosition
	 * @return int[][]
	 */
	public int[][] getAlignedShapePosition(){
		return this.alignedShapePosition;
	}
	
	/**
	 * Sets tetrominoEnum
	 * @param e
	 */
	public void setTetrominoEnum(TetrominoEnum e) {
		this.tetrominoEnum = e; 
		
	}
	
	/**
	 * Gets tetrominoEnum value
	 */
	public TetrominoEnum getTetrominoEnum() {
		int val = tetrominoEnum.getValue();
		return tetrominoEnum.getEnumByValue(val);
	}
	
	/**
	 * Sets board character
	 * @param c
	 */
	public void setBoardChar(char c) {
		this.boardChar = c;
	}
	
	/**
	 * Gets board character
	 * @return
	 */
	public char getBoardChar() {
		return this.boardChar;
	}
	
	/**
	 * gets offset
	 * @return int
	 */
	public int getOffset() {
		return this.rotateOffset;
	}
	
	/**
	 * Sets offset
	 * @param off
	 */
	public void setOffset(int off) {
		this.rotateOffset = off;
		
	}
	
	/* ----=== AUX METHODS ===--- */
	
	/**
	 * Sets alignedShapePosition
	 * Aligns tetromino coordinates to coordinates on board to a specific location (i,j)
	 */
	private final void alignTetToBoard(){
		int len = singleShapePosition.length;
		
		int[][] temp = new int[len][2];
		for(int row = 0; row < len; row++) {
			temp[row][0] = singleShapePosition[row][0] + x;
			temp[row][1] = singleShapePosition[row][1] + y;
		}
		
		this.alignedShapePosition = temp; 
		
	}
	
	/**
	 * Calls alignTetToBoard()
	 */
	public void updateCoordinates() {
		this.alignTetToBoard();
		
	}
	
	/**
	 * 
	 * Rotates tetromino to the right.
	 * 
	 */
	public final void rotateRight()
	{
		int temp = rotateOffset + 1;
		rotateOffset = temp;
		
		if(rotateOffset >= allShapePositions.length) {
			System.out.println("RotateRight");
			rotateOffset = 0;
		}
		this.singleShapePosition = allShapePositions[rotateOffset];
		alignTetToBoard();
		
	}
	
	/**
	 * Rotates tetromino to the left.
	 */
	public final void rotateLeft()
	{
		int t = rotateOffset - 1;
		rotateOffset = t;
		
		if(rotateOffset < 0) {
			System.out.println("rotateleft");
			rotateOffset = allShapePositions.length - 1;
		}
		
		this.singleShapePosition = allShapePositions[rotateOffset];
		alignTetToBoard();
	}
	
	/**
	 * Prints alignedShapePosition
	 */
	public final void printAligned() {
		for(int[] arr : this.alignedShapePosition) {
			for(int num : arr) {
				System.out.print(num + " ");
			}
			System.out.println();
		}
	}
}
