/**
 * @author iyiol
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetrisProject;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.KeyEvent;
import java.io.ObjectInputFilter.Status;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;


/**
 * Contains Menu, New game, Instructions, quit, and pause GUI implementation
 */
public class MenuUI extends TetrisUI {
    
	private static final long serialVersionUID = 1L;
	
	ChatDialogUI dialog = null; 
    JPanel root;
    
    
    public MenuUI(String[] args) {
		
		super(args);
		// TODO Auto-generated constructor stub
	}
   
    /** 
     * 
     * Create a UI consisting of start and stop buttons.
     */
    @Override
    protected JPanel createUI() {
    	
        JPanel control = new JPanel();
        control.setLayout(new FlowLayout(FlowLayout.LEFT));
       
               
        JMenuBar menubar = new JMenuBar();
        control.add(menubar);
        
        JMenu Game = new JMenu("Game");
        
        Game.setMnemonic(KeyEvent.VK_G);
        Game.getAccessibleContext().setAccessibleDescription("Game menu");
        
        Game.addActionListener(e -> {
        	Game.setEnabled(true);
        });
        
        menubar.add(Game);
        
        JMenuItem NewGame = new JMenuItem("New Game");
   
        NewGame.addActionListener(e -> {
            
            NewGame(NewGame);
            NewGame.setEnabled(true);
            
       });
        
        Game.add(NewGame);
        
        JFrame instructionsFrame = new JFrame();
        JMenuItem controlKeyMenu = new JMenuItem("Control Keys");
        
        controlKeyMenu.addActionListener(e -> {
            controlKeys(instructionsFrame);
            controlKeyMenu.setEnabled(true);
        });
        
        Game.add(controlKeyMenu);
        
        JFrame quitFrame = new JFrame();
        JMenuItem quitMenu = new JMenuItem("Quit");
        
        quitMenu.addActionListener(e -> {
            
        	doQuit(quitFrame);
            quitMenu.setEnabled(true);
        });
        
        Game.add(quitMenu);


       

        root = new JPanel();
        root.setLayout(new BorderLayout());
        root.add(control, BorderLayout.NORTH);
        root.add(this, BorderLayout.CENTER);
        root.add(statusBar, BorderLayout.SOUTH);
        status = new JLabel("~~~~~~~~~");
        root.add(status, BorderLayout.SOUTH);
        return root;
    }
    
   /**
    * Controlkey instructions
    * @param frame
    */
    private void controlKeys(JFrame frame){
        JOptionPane.showMessageDialog(frame, "Right Arrow Key: Move tetromino to the right.\n" +
                                             "Left Arrow Key: Move tetromino to the left.\n" +
                                             "Down Arrow Key: Push tetromino down.\n" +
                                             "\"A\" Key: Rotate tetromino right.\n" +
                                             "\"Z\" Key: Rotate tetromino left.\n" +
                                             "Escape Key: Pause-Start game.");
        
    }
    
    /** Creates new game request **/ 
    public void NewGame(JMenuItem Root){
       
          JPanel panel = new JPanel();
          panel.add(new JLabel("Please choose number of players:"));
          
          DefaultComboBoxModel<String> comboModel = new DefaultComboBoxModel<String>();
          
          comboModel.addElement("Single Player");
          comboModel.addElement("Multiplayer");
          
          JComboBox<String> comboBox = new JComboBox<String>(comboModel);
          panel.add(comboBox);

          int result = JOptionPane.showConfirmDialog(null, panel, "Players", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
          switch (result) {
              case JOptionPane.OK_OPTION:
                 
            	  if(comboBox.getSelectedItem() == "Single Player") {
            		  System.out.println("New Game");
            		  control.setGameMode("single");
            		  control.checkGameStatus();
            	  }
            	  
            	  else  if(comboBox.getSelectedItem() == "Multiplayer") {

                      control.setGameMode("multi");
                      dialog = new ChatDialogUI(control);
                      dialog.setVisible(true);
                      dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                     
            	  }
            	  
                  break;
          }
       }
    
    
    /**
     * Quits game
     * @param frame
     */
    private void doQuit(JFrame frame){
    	int input = JOptionPane.showConfirmDialog(frame, "Quit?");
    	
    	if(input == JOptionPane.YES_OPTION) {
    		System.out.println("Quitting");
    		if(control.getGameMode() == "multi") {
    			System.out.println("Closing Connection");
    			control.closeConnection();
    			dialog.setVisible(false);
    			control.model.setOpponentScore(0);
    		}
    		control.ResetGame();
    	}
        
    }
    
    
    /** Prints message to dialog box **/
    public void printMessageToDialog(String s){
    	dialog.printMessage(s);
    	
    }
    
    /** Closes dialog box **/
    public void closeDialog() {
    	dialog.setVisible(false);
    }
    
    /** Writes to status label at bottom of UI **/
    public void writeStatusLabel(String s){
    	status.setText(s);
   
    }
   
}




