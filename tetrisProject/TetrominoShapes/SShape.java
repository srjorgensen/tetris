package tetrisProject.TetrominoShapes;
import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class SShape extends AbstractTetromino{{
	
	final int[][][] S_POSITIONS = new int[][][]{
		{
			{0,0},{0,1},{1,-1},{1,0}
		},
		{
			{-1,-1},{0,-1},{0,0},{1,0}
		},
		{
			{-1,0},{-1,1},{0,-1},{0,0}
		},
		{
			{-1,0},{0,0},{0,1},{1,1}
		}
	};

	setAllPositions(S_POSITIONS);
	setSingleShape(S_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.S);
	setBoardChar('S');

}}
