package tetrisProject.TetrominoShapes;
import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class OShape extends AbstractTetromino{{
	
	final int[][][] O_POSITIONS = new int[][][]{
		{
			{0,0},{0,1},{1,0},{1,1}
		}
	};

	setAllPositions(O_POSITIONS);
	setSingleShape(O_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.O);
	setBoardChar('O');

}}
