package tetrisProject.TetrominoShapes;
import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class ZShape extends AbstractTetromino{{
	
	final int[][][] Z_POSITIONS = new int[][][]{
		{
			{0,-1},{0,0},{1,0},{1,1}
		},
		{
			{-1,0},{0,-1},{0,0},{1,-1}
		},
		{
			{-1,-1},{-1,0},{0,0},{0,1}
		},
		{
			{-1,1},{0,0},{0,1},{1,0}
		}
	};

	setAllPositions(Z_POSITIONS);
	setSingleShape(Z_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.Z);
	setBoardChar('Z');

}}
