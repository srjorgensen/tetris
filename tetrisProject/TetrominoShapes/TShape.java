package tetrisProject.TetrominoShapes;
import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class TShape extends AbstractTetromino{{
	
	final int[][][] T_POSITIONS = new int[][][]{
		{
			{0,0},{1,-1},{1,0},{1,1}
		},
		
		{
			{-1,-1},{0,-1},{0,0},{1,-1}
		},
		
		{
			{0,-1},{0,0},{0,1},{1,0}
		},
		
		{
			{-1,1},{0,0},{0,1},{1,1}
		}
	};

	setAllPositions(T_POSITIONS);
	setSingleShape(T_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.T);
	setBoardChar('T');

}}
