package tetrisProject.TetrominoShapes;

import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class LShape extends AbstractTetromino{{
	
	final int[][][] L_POSITIONS = new int[][][] {
		{
			{0,0},{1,-2},{1,-1},{1,0}
		},
		{
			{-2,-1},{-1,-1},{0,-1},{0,0}
		},
		{
			{-1,0},{-1,1},{-1,2},{0,0}
		},
		{
			{0,0},{0,1},{1,1},{2,1}
		}
	};
	
	setAllPositions(L_POSITIONS);
	setSingleShape(L_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.L);
	setBoardChar('L');

}}
