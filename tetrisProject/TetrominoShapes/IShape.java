package tetrisProject.TetrominoShapes;
import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class IShape extends AbstractTetromino{{
	
	final int[][][] I_POSITIONS = new int[][][]{
		{
			{0,-2},{0,-1},{0,0},{0,1}
		},
		{
			{-2,0},{-1,0},{0,0},{1,0}
		}
	};

	setAllPositions(I_POSITIONS);
	setSingleShape(I_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.I);
	setBoardChar('I');

}}
