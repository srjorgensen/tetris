package tetrisProject.TetrominoShapes;

import tetrisProject.AbstractTetromino;
import tetrisProject.TetrominoFactory.TetrominoEnum;

public class JShape extends AbstractTetromino{{
	
	final int[][][] J_POSITIONS = new int[][][] {
		//Row,col
		{
			{0,0},{1,0},{1,1},{1,2}
		},
		{
			{0,-1},{0,0},{1,-1},{2,-1}
		},
		{
			{-1,-2},{-1,-1},{-1,0},{0,0}
		},
		{
			{-2,1},{-1,1},{0,1},{0,0}
		}
	};
	
	setAllPositions(J_POSITIONS);
	setSingleShape(J_POSITIONS[0]);
	setInitialPoint();
	updateCoordinates();
	setTetrominoEnum(TetrominoEnum.J);
	setBoardChar('J');

}}