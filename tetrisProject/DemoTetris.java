package tetrisProject;


/**
 * Demonstrates running tetris game
 * @author samarahjorgensen
 *
 */
public class DemoTetris {
	/**
	 * Sets up tetris game
	 * @param args
	 */
	public static void setGame(String args[]) {
		GameStateModel model = new GameStateModel();
		
		
		MenuUI UI = new MenuUI(args);
		UI.setModel(model);
		UI.setBoard(model.getBoard());
		
		Controller controller = new Controller(model,UI);
		UI.setController(controller);
		UI.addListener(controller);
		
		UI.run();
		UI.setFocusable(true);
		
	}
		
	public static void main(String[] args){
		
		System.out.println("Testing now...");
		
		setGame(args);

	}

}
