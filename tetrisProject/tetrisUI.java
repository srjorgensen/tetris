package tetrisProject;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JLabel;

import tetrisProject.AnimationApplet;


@SuppressWarnings("serial")
/**
 * Contains the Board UI
 * @author samarahjorgensen
 *
 */
public class TetrisUI extends AnimationApplet{
	
	/**
	 * Width of panel
	 */
	protected static final int DEFAULT_WIDTH = 500;
	/**
	 * Height of panel
	 */
    protected static final int DEFAULT_HEIGHT = 600;
    
    /**
	 * Width of board
	 */
    protected static final int BOARD_WIDTH = 250;
    /**
	 * Width of height
	 */
    protected static final int BOARD_HEIGHT = 500;
    
    /**
     * x coordinate of board on panel
     */
    protected static final int BOARD_X = 20;
    /**
     * y coordinate of board on panel
     */
    protected static final int BOARD_Y = 20;
    
    /**
     * Size of one block on board
     */
    protected static final int BLOCK = 25;
    
	/**
	 * Coordinates of the current tetromino
	 */
	private int[][] tetroCoordinates; 
	
	/**
	 * Coordinates of the next tetromino
	 */
	private int[][] nextTetroCoordinates;
	
    /**
     * The coordinates of the board
     */
    private char[][] boardCoordinates; 
    
    /**
     * Current tetormino
     */
    private AbstractTetromino tetro; 
    
    /**
     * Reference to model
     */
    protected GameStateModel model; 
    
    protected Controller control; 
    
    protected String hostName = " ";
    protected String clientName = " ";
    
    /**
     * Next tetromino
     */
    private AbstractTetromino nextTetro;
    
    private int score = 0;
    private int level = 1;
    private int linesCleared = 0; 
    protected int paused = 0;
    
    
    protected JLabel status; 
    
    /*---=== Colors ===---*/
    
    Color boardColor = new Color(0, 30, 150);
    
    Color wordColor = new Color(255,255,255);
    
    Color lineColor = new Color(0,255,255);
    
    Color borderColor = new Color(255,0,255);
    
    /*-----------------*/
    
    public TetrisUI(String[] args) {
		super(args);
		// TODO Auto-generated constructor stub
	}
    
    /*---=== SETTERS AND GETTERS ===---*/
    /**
     * Sets game state
     * @param m
     */
    public void setModel(GameStateModel m){
    	this.model = m ;
    }
    /**
     * Sets controller
     * @param c
     */
    public void setController(Controller c) {
    	this.control = c; 
    }
    
    /** 
     * Sets game speed. The smaller speed is, the faster the game. 
     * @param speed
     */
    public void setGameSpeed(int speed) {
    	this.delay = speed; 
    }
    
    /**
     * Gets game speed
     * @return
     */
    public int getGameSpeed() {
    	return this.delay; 
    }
	
	/**
	 * Sets boardCoordinates
	 * @param board
	 */
	public void setBoard(char[][] board){
		this.boardCoordinates = board; 
	}
	
	/**
	 * Sets tetromino and tetorminoEnum 
	 */
	public void setTetro(int[][] tArr, AbstractTetromino tetro2) {
		this.tetroCoordinates = tArr; 
		this.tetro = tetro2; 
	}
	
	/**
	 * Sets next tetromino and next tetorminoEnum 
	 */
	public void setNextTetro(int[][] tArr, AbstractTetromino next) {
		this.nextTetroCoordinates = tArr; 
		this.nextTetro = next; 
	}
	
	/**
	 * Sets score
	 * @param s
	 */
	public void setScore(int s) {
		this.score = s;
	}
	
	/**
	 * Sets linesCleared
	 * @param l 
	 */
	public void setLinesCleared(int l) {
		this.linesCleared = l;
	}
	
	public void addToLinesCleared(int n) {
		this.linesCleared += n;
	}
	
	/**
	 * Sets level
	 * @param l
	 */
	public void setLevel(int l) {
		this.level = l;
	}
	
	/** Sets opponent name @param s **/
	public void setClientName(String s) {
		this.clientName = s;
	}
	/** Sets host name @param s **/
	public void setHostName(String s) {
		this.hostName = s;
	}
	/** Sets pause value name @param i **/
	public void setPause(int i) {
		this.paused = i;
	}
	
	/** Gets pause value @return int **/
	public int getPause() {
		return this.paused; 
	}
	
	
	/*---=== AUX METHODS ===---*/
	
	/**
	 * Sets color of block
	 * @param col The character that represents a certain color
	 * @return Color of block
	 */
	
	private Color getTetrominoColor(char col)
	{
		Color color = null;
		switch (col)
		{
		case 'I':
			color = Color.RED; break;
		case 'J':
			color = Color.GREEN; break;
		case 'L':
			color = Color.PINK; break;
		case 'O':
			color  = Color.CYAN; break;
		case 'S':
			color = Color.MAGENTA; break;
		case 'Z':
			color = Color.YELLOW; break;
		case 'T':
			color = Color.ORANGE; break;
		case 'F':
			color = Color.GRAY; break;
		default:
			color =  Color.WHITE; break;
		}//end switch
		return color;
	}//end getTetrominoColor
	
	/**
     * Changes game speed by .1 every time new level happens
     */
    void changeTimerSpeed(){
    	int temp = getGameSpeed(); 
		temp = temp - ((int) (temp * .1));
		setGameSpeed(temp);
    	
    }
	
	/**
	 * Creates game flow
	 */
    @Override 
    public void periodicTask() {
    	
    	control.gameFlow();
    	repaint();
    }
    
	/** Resets board **/ 
	public void resetBoard(){
		for(int i = 0; i < boardCoordinates.length; i++) {
			for(int j = 0; j < boardCoordinates[i].length; j++) {
				boardCoordinates[i][j] = '\u0000';
			}
		}
		
		this.tetroCoordinates = null;
		this.nextTetroCoordinates = null;
		this.tetro = null;
		this.nextTetro = null;
		
		this.hostName = " ";
		this.clientName = " ";
		
		this.score = 0;
		this.level = 1;
		this.linesCleared = 0; 
		
		this.paused = 0;

		
	}
	
	/**
	 * Creates GUI
	 */
	public void paintFrame(Graphics g) {
		
		if(g == null)
			return;
		
		paintBase(g);
		paintConnection(g);
	
		//Paints blocks secured on the board
		paintSetBlocks(g);
		
		paintTetromino(g);
		
		paintNextBox(g);
		
		paintNextTetromino(g);
		
		paintBoardLines(g);
		
		paintStats(g);
		
		if(control.getPaused() == 1) {
			paintPaused(g);
		}
		
		if(control.isGameOver()) {
			paintGameOver(g);
		}
		
		if(control.getGameMode().equals("multi")) {
			paintOpponentStatsAndBoard(g);
		}
		
	}
	
	/** red == no connection, green == connection **/ 
	private void paintConnection(Graphics g) {
		if(control.getOnline() == 0) {
			g.setColor(Color.RED);
		}
		else{
			g.setColor(Color.GREEN);
		};
		
		g.fillRect(530, 0, 20, 20);
	
	}
	
	private void paintOpponentStatsAndBoard(Graphics g) {
		g.setColor(wordColor);
		g.setFont(new Font("Monospaced", Font.ITALIC, 12));
		g.drawString("OPPONENT",325,320);
		g.drawString("SCORE: " + Integer.toString(model.getOpponentScore()),380,340);
		g.drawRect(325,325, 175, 275);
		g.setColor(boardColor);
		g.fillRect(360, 360, 100, 200);
		g.setColor(borderColor);
		g.drawRect(360,360, 100, 200);
	}
	
	/** Paints background and rectangle for tetris board **/
	private void paintBase(Graphics g) {

		g.drawImage(getImage(getCodeBase(),"image/coolStars.jpg"),0, 0, this);
		
		Color boardColor = new Color(0, 30, 150);
		g.setColor(boardColor);
		
		g.fillRect(BOARD_X, BOARD_Y,BOARD_WIDTH,BOARD_HEIGHT);
		
	}
	
	/** Paints users stats **/
	private void paintStats(Graphics g) {
		g.setColor(wordColor);
		
		g.setFont(new Font("Monospaced", Font.ITALIC, 15)); 
		g.drawString("Score: " + score, 300, 175);
		g.drawString("Level: " + level, 300, 200);
		g.drawString("Lines cleared: " + linesCleared, 300, 225);
		
	}
	
	/** Paints paused graphic **/
	private void paintPaused(Graphics g) {
		
		g.setColor(Color.WHITE);
		g.fillRect(95, 245, 100, 50);
		g.setColor(Color.LIGHT_GRAY);
		g.drawString("Paused", 115, 275);
		
	}
	
	/** Paints gameover graphic **/ 
	private void paintGameOver(Graphics g) {
		
		g.setColor(Color.WHITE);
		g.fillRect(95, 245, 100, 50);
		g.setColor(Color.LIGHT_GRAY);
		g.drawString("Game Over", 100, 275);
		
		int[] boardPositions = new int[1];
		
		if(control.isGameOver()) {
			control.winOrLose();
			
		}
	}
	
	/** Paints tetrominos that are set on board **/ 
	private void paintSetBlocks(Graphics g) {
		
		for(int i = 0; i < boardCoordinates.length; i++) {
			for(int j = 0; j < boardCoordinates[i].length; j++) {
				if(boardCoordinates[i][j] != '\u0000') {
					Color setBlockColor = getTetrominoColor(boardCoordinates[i][j]);
					g.setColor(setBlockColor);
					g.fillRect((j*25)+20, (i*25)+20, BLOCK, BLOCK);
				}
			}
		}
		
	}
	/** Paints next tetromino box graphic **/
	private void paintNextBox(Graphics g) {
		
		g.setColor(wordColor);
		g.setFont(new Font("Monospaced", Font.ITALIC, 15)); 
		g.drawString("NEXT", 400, 50);
		g.setColor(boardColor);
		g.fillRect(350, 55,150,100);
		g.setColor(wordColor);
		g.drawRect(350, 55, 150, 100);
		
	}
	
	/** Paints current tetromino **/
	private void paintTetromino(Graphics g) {
		
		if(tetroCoordinates != null) {
			//Colors current tetromino blocks
			for(int i = 0; i < tetroCoordinates.length; i++) {
				Color blockColor = getTetrominoColor(tetro.getBoardChar());
				g.setColor(blockColor);
				g.fillRect((tetroCoordinates[i][1]*25)+20, (tetroCoordinates[i][0]*25)+20, BLOCK, BLOCK);
					
			}
		}
		
	}
	
	/** Paints next tetromino **/ 
	private void paintNextTetromino(Graphics g) {
		
		if(nextTetroCoordinates != null) {
			for(int i = 0; i < nextTetroCoordinates.length; i++) {
				Color blockColor = getTetrominoColor(nextTetro.getBoardChar());
				g.setColor(blockColor);
				g.fillRect((nextTetroCoordinates[i][1]*25)+320, (nextTetroCoordinates[i][0]*25)+80, BLOCK, BLOCK);
			}
		}
		
	}
	
	/** Paints lines on board and board border **/ 
	private void paintBoardLines(Graphics g) {
		
		
		g.setColor(lineColor);
		
		//Draws veritcal lines
		for(int x = BOARD_X; x < BOARD_WIDTH; x += BLOCK) {
			g.drawLine(x, BOARD_Y, x, BOARD_HEIGHT+BOARD_Y);
		}
		//Draws horizontal lines
		for(int y = BOARD_Y; y < BOARD_HEIGHT; y += BLOCK) {
			g.drawLine(BOARD_X, y, BOARD_WIDTH+BOARD_X, y);
		}
		
		g.setColor(borderColor);
		g.drawRect(BOARD_X, BOARD_Y, BOARD_WIDTH, BOARD_HEIGHT);
		
		
	}
	
	/**
	 * Connects controller to UI
	 * @param controller
	 */
	public void addListener(Controller controller) {
		this.addKeyListener(controller);
	}
    
}
