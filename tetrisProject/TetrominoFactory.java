package tetrisProject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import tetrisProject.TetrominoShapes.IShape;
import tetrisProject.TetrominoShapes.JShape;
import tetrisProject.TetrominoShapes.LShape;
import tetrisProject.TetrominoShapes.OShape;
import tetrisProject.TetrominoShapes.SShape;
import tetrisProject.TetrominoShapes.TShape;
import tetrisProject.TetrominoShapes.ZShape;

public class TetrominoFactory extends AbstractTetromino{
	
	private TetrominoEnum tetro = null;
	/**
	 * Makes random tetromino
	 * @return
	 */
	public AbstractTetromino makeTetromino() {
		tetro = TetrominoEnum.getRandomTetromino();

		switch(tetro) {
		case L:
			return new LShape();
		case I:
			return new IShape();
		case J:
			return new JShape();
		case O:
			return new OShape();
		case S:
			return new SShape();
		case Z:
			return new ZShape();
		case T:
			return new TShape();
		default:
			break;
		}
		return null;
		
	}
	
	/**
	 * Makes tetromino from specified TetrominoEnum
	 * @param en
	 * @return
	 */
	public AbstractTetromino makeTetrominoCopy(TetrominoEnum en) {
		TetrominoEnum temp = en;
		
		switch(temp) {
		case L:
			return new LShape();
		case I:
			return new IShape();
		case J:
			return new JShape();
		case O:
			return new OShape();
		case S:
			return new SShape();
		case Z:
			return new ZShape();
		case T:
			return new TShape();
		default:
			break;
		}
		return null;
		
	}
	
	public enum TetrominoEnum 
	{
		/** Types of tetrominos, filler represents punishment lines added in multiplayer mode */
		
		I(0), J(1), L(2), O(3), S(4), Z(5), T(6), FILLER(7);
		
		/** Integer value of each tetromino*/
		private int value;
		
		/**  Hash for inverse lookup of a tetromino based on value*/
		private static final Map<Integer, TetrominoEnum> reverseLookup = new HashMap<Integer, TetrominoEnum>();
		
		static {
			for (TetrominoEnum tetromino : TetrominoEnum.values()) {
				reverseLookup.put(tetromino.getValue(), tetromino);
	        }
		}
		
		
		/**
		 * Constructor that sets the integer value of tetromino 
		 * @param value
		 */
		TetrominoEnum(int value)
		{
			this.value = value;
		}
		
		/**
		 * Return integer value of tetromino
		 * @return
		 */
		public int getValue()
		{
			return value;
		}
		/**
		 * Return TetrominoEnum depending on value
		 * @param value
		 * @return
		 */
		
		public TetrominoEnum getEnumByValue(int value)
		{
			return reverseLookup.get(value);
		}
		
		/**
		 * Returns a random TetrominoEnum
		 * @return
		 */
		public static TetrominoEnum getRandomTetromino() {
            Random random = new Random();
            return values()[random.nextInt(values().length-1)];
        }
	}

}
