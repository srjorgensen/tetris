package tetrisProject;

import java.util.Arrays;
import java.util.Random;

import javax.swing.JLabel;

import tetrisProject.NetworkAdapter;




/**
 * Model class from MVC. This class contains the Tretris logic but no GUI information. 
 * Model must not know anything about the GUI and Controller.
 * @author epadilla2
 *
 */
public class GameStateModel{
	
	/**
	 * Game level
	 */
	private int level = 1;
	
	/**
	 * User score
	 */
	private int score = 0;
	
	/**
	 * Game active state
	 */
	private boolean gameActive = false;
	
	/**
	 * used for tetromino final position
	 */
	private boolean finalActivated = false;
	
	/**
	 * Game over state
	 */
	private boolean gameOver = false;
	
	/**
	 * rows cleared at one time
	 */
	private int rowsCleared = 0;
	
	/**
	 * Total cleared rows
	 */
	private int allClearedRows = 0; 
	
	/**
	 * Next tetromino
	 */
	protected AbstractTetromino nextTetromino; 
	
	/**
	 * Current tetromino
	 */
	protected AbstractTetromino currentTetromino; 
	
	/**
	 * Game board
	 */
	protected char[][] board = new char[20][10];
	
	/**
	 * Opponent board
	 */
	protected char[][] opponentBoard = new char[20][10];
	
	public NetworkAdapter na = null; 
	
	/** Host IP **/
	private String host = " ";
	
	/** Port **/
	private int port = 0; 
	
	private String hostName = " ";
	
	private String clientName = " ";
	
	private int opponentScore = 0;

	private String gameMode = " "; 
	
	/** 0 == not-connected, 1 == connected */
	private int online = 0;
	
	protected JLabel status;

	
	/**
	 * GameState constructor
	 */
	public GameStateModel(){
		
	}
	
	/* ---=== SETTERS AND GETTERS ===--- */
	/**
	 * Sets gameActive
	 * @param b
	 */
	public void setIsGameActive(boolean b) {
		this.gameActive = b;
	}
	
	/**
	 * return gameActive
	 * @return
	 */
	public boolean isGameActive() {
		return gameActive;
	}
	
	/**
	 * Returns gameOver
	 * @return
	 */
	public boolean isGameOver() {
		return this.gameOver ; 
	}
	
	/**
	 * returns finalActivated
	 * @return boolean
	 */
	public boolean isFinalActivated() {
		return finalActivated;
	}
	
	/**
	 * gets board 
	 * @return char[][]
	 */
	public char[][] getBoard() {
		return this.board;
		
	}
	
	/**
	 * Returns score
	 * @return int
	 * 
	 */
	public int getScore()
	{
		return score;
	}
	
	/**
	 * Return current level
	 * @return int
	 * 
	 */
	public int getLevel()
	{
		return level;
	}
	
	/**
	 * sets finalActicated
	 * @param b
	 */
	public void setFinalActivated(boolean b){
		finalActivated = b; 
	}
	
	/**
	 * Gets currentTetormino
	 * @return tetromino
	 */
	public AbstractTetromino getCurrentTetromino(){
		return currentTetromino;
		
	}
	
	/**
	 * sets currentTetormino
	 * @param t
	 */
	public void setCurrentTetromino(AbstractTetromino t) {
		this.currentTetromino = t; 
	}
	/**
	 * Gets the next tetorminos coordinates
	 * @return int[][]
	 */
	public AbstractTetromino getNextTetromino() {
		return this.nextTetromino;
		
	}
	
	/**
	 * Returns allClearedRows
	 * @return
	 */
	public int getAllClearedRows() {
		return allClearedRows;
	}
	
	/** Sets allClearedRow  @param allClearedRows */
	public void setAllClearedRows(int allClearedRows) {
		this.allClearedRows = allClearedRows;
	}
	
	/** Sets host @param h */
	public void setHost(Object h){
		this.host = String.valueOf(h);
		
	}
	/** Gets host @return String **/
	public String getHost(){
		return this.host;
		
	}
	
	/** Sets port @param p */
	public void setPort(Object p) {
		this.port = Integer.valueOf((String) p);
	}
	
	/** Gets port @return int */ 
	public int getPort() {
		return this.port; 
	}
	
	/**Sets host name @param n */
	public void setHostName(Object n) {
		this.hostName = String.valueOf(n);
		System.out.println("Host name: " + hostName);
	}
	
	/** Gets host name*/
	public String getHostName() {
		return this.hostName;
	}
	/** sets Opponent Name @param n **/
	public void setClientName(Object n) {
		this.clientName = String.valueOf(n); 
	}
	
	/** gets opponent name @return String **/
	public String getClientName() {
		return this.clientName;
	}
	
	/** sets networAdapter @param n **/
	public void setNetworkAdapter(NetworkAdapter n) {
		this.na = n; 
	}
	
	/** gets opponentBoard @return char[][] **/
	public char[][] getOpponentBoard(){
		return this.opponentBoard;
	}
	/** sets opponentBoard @param char[][] b **/
	public void setOpponentBoard(char[][] b) {
		this.opponentBoard = b; 
	}
	
	/** gets opponentScore @return int **/ 
	public int getOpponentScore() {
		return this.opponentScore;
	}
	
	/** sets opponent score @param opponentScore **/
	public void setOpponentScore(int opponentScore) {
		this.opponentScore = opponentScore;
	}
	
	/** gets if game is single or multiplayer **/
	public String getMode() {
		return this.gameMode;
	}
	
	/**sets game mode (single/multi) **/
	public void setMode(String mode) {
		this.gameMode  = mode;
		
	}
	
	/** 0 == offline 1 == online **/
	public void setOnline(int i) {
		this.online = i; 
	}
	
	/** returns online **/
	public int getOnline() {
		
		return online;
	}
	
	
	
	/* ---=== AUX METHODS === */
	
	/**
	 * Resets game
	 */
	public void resetGame() {
		this.level = 1;
		this.score = 0;
		this.gameActive = false; 
		this.finalActivated = false;
		this.gameOver = false;
		this.rowsCleared = 0;
		this.allClearedRows = 0; 
		this.host = " ";
		this.port = 0;
		this.online = 0;
	
		
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				board[i][j] = '\u0000';
				opponentBoard[i][j] = '\u0000';
			}
		}
	}
	/**
	 * Creates new game
	 */
	public void newGame() {
		resetGame();
		newPiece();
		gameActive = true; 
		
	}
	
	/**
	 * Draws text version of board
	 */
	public void drawBoard(){
		
		for(int i = 0; i < 20; i ++) {
	
			System.out.println("+---+---+---+---+---+---+---+---+---+---+");
			System.out.print("|");
			
			for(int j = 0; j<10; j++) {
				boolean space = true;
				System.out.print("  ");
				
				//Current board position
				
				int[] pos = {i,j};
				
				for(int[] check :currentTetromino.getAlignedShapePosition())
					//Checks if tetormino position is equal to board position to print char
					if(Arrays.equals(pos,check)){
						System.out.print(currentTetromino.getBoardChar());
						space = false;
					}
				
				if(board[i][j] != '\u0000') {
					System.out.print(board[i][j]);
					space = false; 
				}
				
				if(space) {
					System.out.print(" ");
				}
				
				
				System.out.print("|");
			}
			
			System.out.println();

		}
		System.out.println("+---+---+---+---+---+---+---+---+---+---+");
	}
	
	/**
	 * Moves tetromino down
	 */
	public void moveTetrominoDown()
	{
		
		boolean check = validateMoveDown(currentTetromino);
		
		if(check) {
			int currentX = currentTetromino.getX();
			currentTetromino.setX(currentX += 1);
			currentTetromino.updateCoordinates();
		}
	
	}
	
	/**
	 * adds to score 5 points
	 */
	private void addScore(){
		score += 5; 
		
	}
	
	/**
	 * Moves tetromino left
	 */
	public void moveTetrominoLeft()
	{
		System.out.println("Move left");
		int currentY = currentTetromino.getY();
		AbstractTetromino temp  = createTempTetromino(currentTetromino);
		
		
		temp.setY(currentY - 1);
		temp.updateCoordinates();
		
		
		if(validateTetrominoPosition(temp)) {
			System.out.println("VALID");
			currentTetromino.setY(currentY - 1);
			currentTetromino.updateCoordinates();
				
		}
	}
	
	/**
	 * Moves tetromino right
	 */
	public void moveTetrominoRight()
	{
		System.out.println("Move right");
		int currentY = currentTetromino.getY();
		AbstractTetromino temp = createTempTetromino(currentTetromino);
		
		temp.setY(currentY + 1);
		temp.updateCoordinates();
		
		currentTetromino.printAligned();
		if(validateTetrominoPosition(temp)) {
			System.out.println("VALID");
			currentTetromino.setY(currentY + 1);
			currentTetromino.updateCoordinates();
			
		}
	}
	
	/**
	 * Rotates tetromino left
	 */
	public void tryRotateLeft() {
		if(!isFinalActivated()) {
			
			System.out.println("Rotate left");
			AbstractTetromino temp = createTempTetromino(currentTetromino);
			
			temp.rotateLeft();
			
			if(validateTetrominoPosition(temp)) {
				System.out.println("VALID");
				currentTetromino.rotateLeft();
			}
		}
	}
	
	/**
	 * Rotates tetromino right
	 */
	public void tryRotateRight() {
		if(!isFinalActivated()) {
			
			System.out.println("Rotate right");
			AbstractTetromino temp = createTempTetromino(currentTetromino);
			
			temp.rotateRight();
			
			if(validateTetrominoPosition(temp)) {
				System.out.println("VALID");
				currentTetromino.rotateRight();
			}
		}	
	}
	
	/** Creates temporary tetromino based off of tet @param AbstractTetromino tet **/
	private AbstractTetromino createTempTetromino(AbstractTetromino tet) {
		
		TetrominoFactory fac = new TetrominoFactory();
		
		AbstractTetromino temp = fac.makeTetrominoCopy(tet.getTetrominoEnum());
		
		temp.setSingleShape(tet.getSingleShape());
		
		temp.setX(tet.getX());
		temp.setY(tet.getY());
		
		temp.setAlignedShapePosition(tet.getAlignedShapePosition());
		
		temp.setOffset(tet.getOffset());
		
		return temp; 
	}
	
	/**
	 * Creates new piece
	 * @return
	 */
	protected boolean newPiece() {
		TetrominoFactory factory = new TetrominoFactory();
		
		if(nextTetromino == null) {
			currentTetromino = factory.makeTetromino();
		}
		else {
			currentTetromino = nextTetromino;
		}
		nextTetromino = factory.makeTetromino();
		
		if(!validateTetrominoPosition(currentTetromino)) {
			System.out.println("Game Over");
			int[] boardPositions = new int[1];
			if(gameMode.equals("multi")) { na.writeStatus(score, 1, boardPositions); }; 
			gameActive = false; 
			gameOver = true; 
			return false; 
		}
		return true;
		
	}
	
	
	/**
	 * Places tetromino in final position on board
	 */
	public void finalPosition() {
		
		
		if(finalActivated) {
			int[] boardPositions = new int[1];
			if(gameMode.equals("multi")) { na.writeStatus(score, 0, boardPositions ); }; 
			addScore();
			plotTetro();
			
		}
		finalActivated = false;
		
	}
	
	/**
	 * Plots coordinates on board 
	 */
	private void plotTetro() {
		System.out.println("Plot tetro");
		for(int i = 0; i < currentTetromino.getAlignedShapePosition().length; i++){
			int a = currentTetromino.getAlignedShapePosition()[i][0];
			int b = currentTetromino.getAlignedShapePosition()[i][1];
			board[a][b] = currentTetromino.getBoardChar(); 
		}
	}
	
	/**
	 * Returns if this is a valid position
	 * @return true if valid position, false otherwise 
	 */
	public boolean validateTetrominoPosition(AbstractTetromino currentTetromino2){
		
		
		for(int[] x : currentTetromino2.getAlignedShapePosition()) {
			
			int a = x[0];
			int b = x[1];
			
			//Out of range of board
			if(a >= 20 || a < 0 || b > 9|| b < 0) {
				System.out.println("OUT OF RANGE");
				return false;
			}
			else if(a >= 19) {
				System.out.println("last row");
				finalActivated = true;
				return false;
			}
			else if(board[a+1][b] != '\u0000') {
				System.out.println("Hit another tetormino ");
				//Used in finalPosition()
				
				finalActivated = true; 
				return false; 
				
			}
			
		
		}

		return true;
	}
	
	/**
	 * Validation for moveTetrominoDown()
	 * @param temp
	 * @return
	 */
	public boolean validateMoveDown(AbstractTetromino temp) {
		for(int[] x : temp.getAlignedShapePosition()) {

			int a = x[0];
			int b = x[1];
			
			if(a >= 19) {
				System.out.println("last row");
				finalActivated = true;
				return false;
			}
			else if(board[a+1][b] != '\u0000') {
				System.out.println("Hit another tetormino ");
				//Used in finalPosition()
				finalActivated = true; 
				return false; 
				
			}
			
		}
		return true; 
	}
	
	/**
	 * deletes row on board
	 * @param row
	 */
	public void deleteRow(int row) {
		System.out.println("Delete row" + row);
		for (int i = row; i > 1; i--) {
			for (int j = 0; j < 10; j++) {
				board[i][j] = board[i-1][j];
			}
		}
	}  

	/**
	 * Checks for rows to clear on board
	 * @return
	 */
	public int clearRows() {
		System.out.println("Checking for cleared Rows");
		boolean space;
		
		int tempRowsCleared = 0;
		
		for (int i = 0; i < 20; i++) {
			space = false;
			for (int j = 0; j < 10; j++) {
				if (board[i][j] == '\u0000') {
					space = true;
					break;
				}
			}
			
			if (!space) {
				for(int k = 0; k < 10; k++) {
					System.out.println(board[i][k] + " ");
				}
				deleteRow(i);
				tempRowsCleared += 1;
				rowsCleared += 1; 
			}
		}
		
		
		switch (tempRowsCleared) {
		
			case 1:
				score += 100;
				na.writeFill(1);
				break;
			case 2:
				score += 300;
				na.writeFill(1);
				break;
			case 3:
				score += 600;
				na.writeFill(1);
				break;
			case 4:
				score += 1000;
				na.writeFill(1);
				break;
		}
		setAllClearedRows(getAllClearedRows() + tempRowsCleared); 
		return tempRowsCleared; 
	}
	
	/**
	 * Changes level ever 5 rows cleared
	 * @return
	 */
	public boolean changeLevel() {
		if(rowsCleared == 5) {
			rowsCleared = 0; 
			level += 1; 
			return true; 
		}
		return false;
		
	}
	
	/**
	 * Creates filler lines
	 * @param num
	 */
	public void fill(int num){
		Random rand = new Random(); 
		for(int i = 0; i < 20; i++) {
			for(int j = 0; j < 10; j++) {
				if(board[i][j] != '\u0000') {
					if(i-num > 0) {
						board[i-num][j] = board[i][j];
						board[i][j] = '\u0000';
					}
				}
			}
		}
		for(int i = 0; i < num; i++) {
			int random =rand.nextInt(10);
			for(int j = 0; j < 10; j++) {
				if(j != random) {
					board[20-num][j] = 'F';
				}
			}
		}
	}
}