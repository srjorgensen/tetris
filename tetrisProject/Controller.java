package tetrisProject;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import tetrisProject.NetworkAdapter.MessageType;


/**
 * Manages all events between View (GUI) and Model (Game State)
 * @author epadilla2
 *
 */
public class Controller implements KeyListener, Runnable, NetworkMessageListener{


	protected GameStateModel model;
	protected MenuUI view;
	
	protected NetworkAdapter na = null;
	protected Socket ss = null; 
	
	private int paused = 0; 
	
	
	/**
	 * Controller constructor
	 * @param model
	 * @param view
	 */
	public Controller(GameStateModel model, MenuUI view){

		
		this.model = model;
		this.view = view; 
		
	}	
	/* ---=== Setters and Getters ===--- */
	/** Sets model **/
	public void setModel(GameStateModel m) {
		this.model = m; 
		
	}
	
	/** Sets view **/
	public void setView(MenuUI v) {
		this.view = v; 
	}
	
	/** gets paused value **/
	public int getPaused() {
		return this.paused;
	}
	/** sets gameMode (single/multi) **/
	public void setGameMode(String mode) {
		model.setMode(mode);
		
	}
	
	/** gets gameMode (single/multi) **/
	public String getGameMode() {
		return model.getMode();
		
	}
	
	/**Sets host IP and port in model**/
	public void setHostPort(Object host, Object port){
		model.setHost(host);
		model.setPort(port);
		
	}
	
	/** returns gameOver **/
	public boolean isGameOver() {
		
		return model.isGameOver();
	}
	
	/** Checks if network connection is online **/
	public int getOnline() {
		return model.getOnline();
	}
	
	/* ---=== KEY EVENT ===--- */
	
	/**
	 * Defines action when a key is pressed
	 */
	@Override
	public void keyPressed(KeyEvent e) {
	
		int keyCode = e.getKeyCode();
	
	
		if  (keyCode == KeyEvent.VK_ESCAPE){
            pause();
            return;
        }	
		
		
		switch(keyCode) 
		{ 
			case KeyEvent.VK_DOWN:
				
				model.moveTetrominoDown();
				view.repaint();
				break;
				
			case KeyEvent.VK_LEFT:
				model.moveTetrominoLeft();
				view.repaint();
				break;
			case KeyEvent.VK_RIGHT :
				model.moveTetrominoRight();
				view.repaint();
				break;
			case KeyEvent.VK_Z:
				model.tryRotateLeft();
				view.repaint();
				break;
			case KeyEvent.VK_C:
				model.tryRotateRight();
				view.repaint();
				break;
			default:
				break;
		}
		
	}
	

	/* ---===  AUX METHODS ===--- */
	
	/** Resets game **/
	public void ResetGame() {
		paused = 0; 
		model.resetGame();
		view.resetBoard();
		
	}
	
	/** Closes network connection **/
	public void closeConnection() {
		na.writeQuit();
		
	}
	
	/**
	 * Sets up model for either host or connector mode
	 * @param command
	 */
	public void dialogButton(String command, Object name) {
	
		if(command.equals("connect")) {
			System.out.println("Connect button pressed");
			
			model.setClientName(name);
			view.setClientName(name.toString());
			setConnection();
			
			view.printMessageToDialog("Hello " + name + " (connect)");
			
			
		}
		if(command.equals("host")) {
			System.out.println("Host button pressed");
			
			model.setHostName(name);
			view.setHostName(name.toString());
			
			setHost();
			
			view.printMessageToDialog("Hello " + name + " (host)");
			view.printMessageToDialog("Looking for connections");
			
			
		}
	}
	
	
	/** Creates game request **/
	public boolean newGameRequest(String s){
		
    	boolean choice = false;
    	
    	String[] choices = {"Accept", "Reject"};
    	
    	JFrame frame = new JFrame();
    	Object answer = JOptionPane.showInputDialog(frame, s + " wants to start a game!", 
    			"Game Request", JOptionPane.QUESTION_MESSAGE,null, choices, choices[0]);
    	
    	if(answer.equals("Accept")){
    		System.out.println(view == null);
    		choice = true; 
    		
    	}
    	
		return choice;
  
    }
	
	/** Creates new multiplayer game **/
    private void newMultiGame() {
		view.dialog.printMessage("Game Accepted");
		view.dialog.printMessage("Game starting in...");
		for(int i = 3; i > 0; i--) {
			view.dialog.printMessage(Integer.toString(i));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		view.dialog.printMessage("~~~Chat Started~~~");
		checkGameStatus();
	}
	
	/**
	 * Checks to see if game is on or off
	 */
	public void checkGameStatus() {
		if(!model.isGameActive()) {
				view.start();
				
		}
		model.newGame();
		if(model.getMode() == "multi")
			model.setOnline(1);
	}
	
	/**
     * Pauses game
     */
    private void pause() {
    	if(model.isGameActive()) {
	    	paused += 1; 
	    	paused = paused % 2; 
	    	if(paused == 0) {
	    		System.out.println("Start");
	    		view.start();
	    	}
	    	if(paused == 1) {
	    		System.out.println("Stop");
	    		view.repaint();
	    		view.stop();
	    	}
    	}
    }
 
	
    /** The flow of the game**/
	public void gameFlow() {
		if(model.isGameActive()) {
    		
	    	if(!model.isFinalActivated()) {
	    		
	    		if(model.getCurrentTetromino() != null) {
	    			
	    			AbstractTetromino next = model.getNextTetromino();
		    		view.setNextTetro(next.getAlignedShapePosition(), next);
		    		
		    		view.setTetro(model.getCurrentTetromino().getAlignedShapePosition(), model.getCurrentTetromino());
	        		view.setScore(model.getScore());
	        		
	        		view.repaint();
	        		
	    			model.moveTetrominoDown();
	        		
	    		}
	    		
	    	}
	    	
	    	else {
	    		
	    		model.finalPosition();
	    		
	    		view.addToLinesCleared(model.clearRows());
	    		view.setBoard(model.getBoard());
	    		
	    		if(model.changeLevel()) {
	    			view.changeTimerSpeed();
	    			view.setLevel(model.getLevel());
	    		}
	    		
	    		
	    		boolean check = model.newPiece();
	    		if(!check) {
	    			int[] boardPositions = new int[1];
					na.writeStatus(model.getScore(), 1, boardPositions );
	    			view.repaint();
	    			System.out.println("End");
	    		}
	    		
	    		model.setFinalActivated(false);
	    	}
    	}
	}
	
	/** Sets statusLabel with users game result **/ 
	public void winOrLose() {
		if(model.getOpponentScore() < model.getScore()) {
			view.writeStatusLabel("you won :)");
		}
		if(model.getOpponentScore() > model.getScore()) {
			view.writeStatusLabel("you lost :(");
		}
		if(model.getScore() == model.getOpponentScore()) {
			view.writeStatusLabel("tie");
		}
	}
	
	/* --=== NETWORK METHODS ===--- */
	
	/** Sets connectors connection **/
	@SuppressWarnings("resource")
	public void setConnection(){
		
		boolean check = false; 
		
		System.out.println("You are client");
		try {
			ss = new Socket(model.getHost(), model.getPort());
			check = true; 
			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			view.dialog.printMessage("Connection failed, try again! " + e.getMessage());
		}
	
		if(check) {
			setNetworkAdapterAndRecieve(ss);
		}
		
		na.writeNew(model.getClientName());
		
	}
	
	/** Sets server connection **/
	public void setHost(){
		
		System.out.println("You are server");
		System.out.println("Waiting for connections...");

		Runnable run = () ->{
			ServerSocket server = null; 
			try {
				server = new ServerSocket(model.getPort());
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				view.dialog.printMessage("Exception: " +  e.getMessage());
			}
			
			
			try {
		
				ss = server.accept();
				
				setNetworkAdapterAndRecieve(ss);
				
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				view.dialog.printMessage("Exception: " +  e.getMessage());
			}
		};
		
		Thread t = new Thread(run);
		t.start();
	}
	
	/** 
	 * Sets up the listener for NetworkAdapter, calls recieveMessageAsycn, and sets up the NetworkAdapter in model 
	 * @param s
	 **/
	private void setNetworkAdapterAndRecieve(Socket s) {
		
		System.out.println("SET NETWORKADAPTER");
		na = new NetworkAdapter(s);
		na.setMessageListener(this :: messageReceived);
		na.receiveMessagesAsync();
		model.setNetworkAdapter(na);
	}
	

	
	@Override
	public void messageReceived(MessageType type, String s, int x, int y, int z, int[] others) {
		
		System.out.println("IN MESSAGE RECIEVED");
		boolean newGame; 
		
		switch(type) {
			case NEW:
				newGame = newGameRequest(s);
				
				na.writeNewAck(s,newGame ? 1 : 0);
				
				if(newGame) {
					newMultiGame();
					
				}
				else {
					na.close();
					view.printMessageToDialog("Game request rejected!");
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					view.closeDialog();
				}
				
				break;
				
			case NEW_ACK:
				
				System.out.println("NEW ACK");
				
				if(x == 1) {
					newMultiGame();
				}
				
				break;
				
			case STATUS:
				System.out.println("STATUS");
				this.model.setOpponentScore(x);
				
				
				this.view.repaint();
				
				if(y == 1 && this.model.isGameOver()) {
					winOrLose();
				}
				if(model.getOpponentScore() < model.getScore()) {
					System.out.println("Winning :)");
					view.writeStatusLabel("you are winning :)");break;
				}
				else {
					System.out.println("Losing :)");
					view.writeStatusLabel("you are losing :(");break;
					
				}
			
			case FILL:
				System.out.println("Fill");
				if(!this.model.isGameOver()) {
					model.fill(x);
				}
				break;
				
			case MESSAGE:
				view.printMessageToDialog(s);
				
			case UNKNOWN:
				System.out.println("UNKNOWN");
				break;
				
			case QUIT:
				System.out.println("QUIT");
				
				view.printMessageToDialog("Player Quit");
		        if (na != null) {
		          
		          Socket socket = na.socket();
		          if (socket != null && !socket.isClosed())
		          {
		            na.writeQuit();
		          }
		          model.setOnline(0);
		          na.close();
		        } 
		        
		        
				break;
		}
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}
}

